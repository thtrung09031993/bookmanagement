'use strict';
var mongoose = require('mongoose');// implement mongoose
var  bookSchema = new mongoose.Schema({
        name: String,
        author: String,
        description: String,
        price: Number
    },
    {
        collection: 'bookCol'
    });

module.exports = mongoose.model('Book', bookSchema); //export book schema