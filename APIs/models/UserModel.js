'use strict';
var mongoose = require('mongoose');// implement mongoose
// var userSchema = new  mongoose.Schema({ //delcare schema for a user
//     username : {
//         type:String,
//         unique: true
//     },
//     password: String
// },
//     {
//         collection: 'userCol'
//     });

var userSchema = new mongoose.Schema({ // Schema for account with email verification
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String
    },
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    authToken: { //need an authorization token
        type: String,
        required: true,
        unique: true
    },
    isAuthenticated: {// check if user verified the email or not
        type: Boolean,
        required: true
    }
});

module.exports = mongoose.model('User', userSchema);
