'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    nodemailer = require('nodemailer'),
    crypto = require('crypto'); //crypto hash module
// sendgrid = require('@sendgrid/mail');
// const sendgrid = require('sendgrid')('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');
// const APIkey = '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';
// sendgrid.setApiKey(APIkey);

//Render the login page
exports.go_to_login = function (req, res) {
    res.render('login');
}

//Login
exports.login = function (req, res) {
    User.findOne({ email: req.body.email }, function (err, user) {
        if (err) {
            res.send('Database error!');
        }
        else {
            if (user === null) {
                res.render('login', { stt: "error", message: "Your account doesn't exist!" });
            } else {
                if (!user.isAuthenticated) {
                    res.render('login', { stt: "error", message: "Please go to email to verify your account 1st!" });
                }
                else {
                    if (user.password !== req.body.password) {
                        res.render('login', { stt: "error", message: "Wrong password!" });
                    } else {
                        req.session.user = user.firstname + ' ' + user.lastname;
                        res.redirect('/book/index');
                    }
                }
            }
        }
    });
}

//Logout
exports.logout = function (req, res) {
    req.session.destroy(function () {
        res.redirect('/user/logout');
    });
}

//Render the registration page
exports.go_to_reg = function (req, res) {
    res.render('reg');
}

//Create a new user
// exports.create_a_user = function (req, res) {
//     var info = req.body;//get the parsed-to-json body
//     if (!info.username || !info.password) { // validate the empty field
//         res.send('Dont let empty field');
//     } else {
//         var newUser = new User({//connect to the schema for executing methods
//             username: info.username,
//             password: info.password
//         });
//         newUser.save(function (err, User) {//run the save method to create new document in db, Person is required parameter
//             if (err) {
//                 res.render('reg', {stt: "error", message: "User existed!"});
//             } else {
//                 res.render('reg', {stt: "new", message: "Register successfully!"});
//             }
//         });
//     }
// }


//make a user with email verification sending to user's email
exports.create_a_user = function (req, res) {
    //generate auth token
    var seed = crypto.randomBytes(20);//encode
    var authToken = crypto.createHash('sha1').update(seed + req.body.email).digest('hex');//encode
    var newUser = new User({
        email: req.body.email,
        password: req.body.password,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        authToken: authToken,
        isAuthenticated: false
    });
    newUser.save(function (err) {
        if (err) {
            res.render('reg', { stt: "error", message: "User existed!" });
        } else {
            var authURL = 'http://localhost:3000/user/verify_email?token=' + authToken;// this is the URL to
            //send email verification for users.
            const emailHtml = '<h1>Confirm your email</h1>'
                + 'Go the following link to confirm your email:'
                + '<a target = _blank href = \"' + authURL + '\">Verify your email</a>';
            
            let transporter = nodemailer.createTransport({
                service:'gmail',                
                 // true for 465, false for other ports
                auth: {
                    user: 'thtrung9316@gmail.com', // generated ethereal user
                    pass: '09031993'  // generated ethereal password
                },
            });

            // setup email data with unicode symbols
            let mailOptions = {
                from: 'trung@abc.com', // sender address
                to: req.body.email, // list of receivers
                subject: 'Confirm', // Subject line
                text: 'Read email', // plain text body
                html: emailHtml// html body
            };
            // send mail with defined transport object
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    return console.log(error);
                }
                res.render('reg', { stt: "new", message: "Register successfully, please confirm your email before logging in!" });
                console.log('Message sent: %s', info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
                // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
            });
        }
    });
}

exports.verify_email = function (req, res) {
    User.findOne({ authToken: req.query.token }, function (err, user) {
        if (!user) {
            res.send("Can't verify your email!");
        }
        else {
            user.isAuthenticated = true;//after find the user with auth token, make that user authenticated
            user.save(function (err) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('auth');
                }
            })
        }
    });
}

exports.checkUserLogin = function(req,res,next){
    if (req.session.user){
        next();
    }
    else {
        res.redirect('/');
    }
}

