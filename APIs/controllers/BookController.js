'use strict';

var mongoose = require('mongoose'),
    Book = mongoose.model('Book');

//Show all the books
exports.show_all_books = function (req,res) {
    Book.find(function (err, books) {
        res.render('index', {books: books, user:req.session.user});
    });
}

//Render the adding book page
exports.go_to_add = function (req,res) {
    res.render('add');
}

//Create a new book
exports.create_a_book = function (req,res) {
    var newBook = new Book({
        name: req.body.name,
        author: req.body.author,
        description: req.body.description,
        price: req.body.price
    });
    newBook.save(function (err) {
        if (err) {
            res.render('add', {stt: "error", message: "Error while adding new book!"});
        } else {
            res.redirect('/index');
        }
    });
}

//Render the update book page
exports.go_to_update = function (req,res) {
    Book.findById(req.params.id, function (err, book) {
        if (!err) {
            res.render('update', {book: book});
        }
    });
}

//Update a book
exports.update_a_book = function (req,res) {
    Book.findByIdAndUpdate(req.body.id, req.body, function (err) {
        if (err) {
            res.render('update', {stt: "error", message: "Error while updating!"});
        } else {
            res.redirect('/index');
        }
    });
}

//Delete a book
exports.delete_a_book = function (req,res) {
    Book.findByIdAndRemove(req.params.id, function (err) {
        if (!err) {
            res.redirect('/index');
        }
    });
}