'use strict';
module.exports = function (app) {
    var user = require('../controllers/UserController');

    //user routes
    //Login
    app.route('/')
        .get(user.go_to_login);
    app.route('/user/login')
        .post(user.login);

    //Register
    app.route('/user/reg')
        .get(user.go_to_reg)
        .post(user.create_a_user);
    app.route('/user/verify_email')
        .get(user.verify_email);

    //Logout
    app.route('/user/logout')
        .get(user.logout);
}