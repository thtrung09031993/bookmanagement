'use strict';
module.exports = function (app) {
    var book = require('../controllers/BookController');
    var user = require('../controllers/UserController');

    //apply the session checking middleware to book's routes
    app.use('/book/*', user.checkUserLogin)

    //Book routes
    //Loading book list
    app.route('/book/index')
        .get(book.show_all_books);

    //Add new book
    app.route('/book/add')
        .get(book.go_to_add)
        .post(book.create_a_book);

    //Update book
    app.route('/book/update/:id')
        .get(book.go_to_update);
    app.route('/book/update')
        .post(book.update_a_book);


    //Delete book
    app.route('/book/delete/:id')
        .get(book.delete_a_book);
}